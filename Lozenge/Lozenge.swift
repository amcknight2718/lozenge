//
//  Lozenge.swift
//  Lozenge
//
//  Created by Andrew McKnight on 10/8/16.
//  Copyright © 2016 Dark Sky. All rights reserved.
//

import UIKit

class Lozenge: UIView {

    var topDome: LozengeDome!
    var bottomDome: LozengeDome!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    convenience init(backgroundColor: UIColor) {
        self.init()
        self.backgroundColor = backgroundColor
        commonInit()
    }

}

// MARK: Public

extension Lozenge {

    func setProgress(progress: CGFloat) {
        for dome: LozengeDome in [ topDome, bottomDome ] {
            dome.setProgress(progress: progress)
        }
        layoutSubviews()
    }
    
}

// MARK: Private

extension Lozenge {

    func commonInit() {
        topDome = LozengeDome(backgroundColor: backgroundColor, tintColor: tintColor)
        bottomDome = LozengeDome(backgroundColor: backgroundColor, tintColor: tintColor)

        let body = UIView()
        body.backgroundColor = tintColor

        for view in [topDome, body, bottomDome] {
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(view)
        }

        topDome.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topDome.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        topDome.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topDome.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5).isActive = true

        bottomDome.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        bottomDome.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        bottomDome.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomDome.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5).isActive = true

        body.topAnchor.constraint(equalTo: topDome.bottomAnchor).isActive = true
        body.bottomAnchor.constraint(equalTo: bottomDome.topAnchor).isActive = true
        body.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 1).isActive = true
        body.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1).isActive = true

        bottomDome.transform = CGAffineTransform(scaleX: 1, y: -1)
    }

}
