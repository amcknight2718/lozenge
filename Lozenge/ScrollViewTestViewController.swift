//
//  ScrollViewTestViewController.swift
//  Lozenge
//
//  Created by Andrew McKnight on 10/15/16.
//  Copyright © 2016 Dark Sky. All rights reserved.
//

import UIKit

class ScrollViewTestViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    var containerView: UIView!
    var lozenges = [Lozenge]()
    var lozengeContainerViews = [UIView]()
    var spacerViewHeightConstraints = [UIView: NSLayoutConstraint]()

    fileprivate let distanceFromCenterToBeginMergingLozenges: CGFloat = 60
    fileprivate let distanceOverWhichToMergeLozenges: CGFloat = 50

    fileprivate let spacerHeight: CGFloat = 20
    fileprivate let distanceFromCenterToBeginClosingLozengeSpace: CGFloat = 30
    fileprivate let distanceOverWhichToCloseLozengeSpace: CGFloat = 30

    override func viewDidLoad() {
        super.viewDidLoad()

        self.buildScrollView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = containerView.bounds.size
        self.setProgress()
    }

}

extension ScrollViewTestViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.setProgress()
    }

}

private extension ScrollViewTestViewController {

    func buildScrollView() {
        let numViews = 50
        var lastSpacerView: UIView?
        var totalHeight: CGFloat = 0
        containerView = UIView(frame: .zero)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        for i in 0 ..< numViews {
            let height = CGFloat(Int(arc4random_uniform(50)) + 50)
            let lozengeContainerView = createLozengeContainerView(height: height, top: i == 0, bottom: i == numViews - 1, lastSpacerView: lastSpacerView)
            if i < numViews - 1 {
                lastSpacerView = createSpacerView(belowLozengeContainerView: lozengeContainerView)
            }
            totalHeight += height
        }
        containerView.heightAnchor.constraint(equalToConstant: totalHeight).isActive = true
        scrollView.addSubview(containerView)
        containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true

        scrollView.delegate = self
    }

    func createLozengeContainerView(height: CGFloat, top: Bool, bottom: Bool, lastSpacerView: UIView?) -> UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(view)
        if top {
            view.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        } else if bottom {
            view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        } else {
            view.topAnchor.constraint(equalTo: lastSpacerView!.bottomAnchor).isActive = true
        }

        view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true

        view.heightAnchor.constraint(equalToConstant: height).isActive = true

        let lozenge = Lozenge(backgroundColor: .white)
        lozenge.translatesAutoresizingMaskIntoConstraints = false
        lozenge.tintColor = .blue
        view.addSubview(lozenge)
        lozenge.widthAnchor.constraint(equalToConstant: 20).isActive = true
        lozenge.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        lozenge.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        lozenge.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        lozenges.append(lozenge)
        lozengeContainerViews.append(view)
        
        return view
    }

    func createSpacerView(belowLozengeContainerView lozengeContainerView: UIView) -> UIView {
        let spacerView = UIView(frame: .zero)
        spacerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(spacerView)
        spacerView.topAnchor.constraint(equalTo: lozengeContainerView.bottomAnchor).isActive = true
        spacerView.leadingAnchor.constraint(equalTo: lozengeContainerView.leadingAnchor).isActive = true
        spacerView.trailingAnchor.constraint(equalTo: lozengeContainerView.trailingAnchor).isActive = true
        let constraint = spacerView.heightAnchor.constraint(equalToConstant: spacerHeight)
        constraint.isActive = true
        spacerViewHeightConstraints[spacerView] = constraint
        return spacerView
    }

    func setProgress() {
        let centerY = view.bounds.midY

        for (spacer, heightConstraint) in spacerViewHeightConstraints {
            let spacerCenterY = spacer.convert(CGPoint.zero, to: view).applying(CGAffineTransform(translationX: 0, y: -20)).y
            let spacerDistanceFromCenter = fabs(centerY - spacerCenterY)
            var progress: CGFloat = 1 - (spacerDistanceFromCenter - distanceFromCenterToBeginClosingLozengeSpace) / distanceOverWhichToCloseLozengeSpace
            if progress < 0 { progress = 0 }
            if progress > 1 { progress = 1 }
            heightConstraint.constant = spacerHeight * progress
        }

        for lozenge in lozenges {
            let lozengeTopDomeTipY = lozenge.convert(CGPoint.zero, to: view).applying(CGAffineTransform(translationX: 0, y: -20)).y
            let lozengeTopDomeDistanceFromCenter = fabs(centerY - lozengeTopDomeTipY)
            let topProgress = (lozengeTopDomeDistanceFromCenter - distanceFromCenterToBeginMergingLozenges) / distanceOverWhichToMergeLozenges;
            lozenge.topDome.setProgress(progress: topProgress)

            let lozengeBottomDomeTipY = lozenge.convert(CGPoint(x: 0, y: lozenge.bounds.maxY), to: view).applying(CGAffineTransform(translationX: 0, y: -20)).y
            let lozengeBottomDomeDistanceFromCenter = fabs(centerY - lozengeBottomDomeTipY)
            let bottomProgress = (lozengeBottomDomeDistanceFromCenter - distanceFromCenterToBeginMergingLozenges) / distanceOverWhichToMergeLozenges;
            lozenge.bottomDome.setProgress(progress: bottomProgress)
        }
    }

}
