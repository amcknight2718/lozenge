//
//  DomeCurve.swift
//  Lozenge
//
//  Created by Andrew McKnight on 10/8/16.
//  Copyright © 2016 Dark Sky. All rights reserved.
//

import UIKit

class DomeCurve: UIView {

    var cp1XDelta: CGFloat = 0
    var cp1YDelta: CGFloat = 0
    var cp2XDelta: CGFloat = 1
    var cp2YDelta: CGFloat = 0

    var ep1XDelta: CGFloat = 0
    var ep1YDelta: CGFloat = 1
    var ep2XDelta: CGFloat = 1
    var ep2YDelta: CGFloat = 0

    var progress: CGFloat?

}

// MARK: Init

extension DomeCurve {

    convenience init(backgroundColor: UIColor?, tintColor: UIColor) {
        self.init()
        self.backgroundColor = backgroundColor
        self.tintColor = tintColor
    }
}

// MARK: Drawing

extension DomeCurve {

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        let firstEndpointXClipCorrection: CGFloat = 1
        var secondEndpointXClipCorrection: CGFloat = 0
        if progress != nil {
            cp1YDelta = cp1Y(progress: progress!)
            cp2XDelta = cp2X(progress: progress!)
            cp2YDelta = cp2Y(progress: progress!)
            ep2XDelta = ep2X(progress: progress!)

            if progress! > 1 {
                secondEndpointXClipCorrection = 1
            }
            else if progress! < 0 {
                secondEndpointXClipCorrection = 0
            }
            else {
                secondEndpointXClipCorrection = progress!
            }
        }

        let firstEndpoint = bounds.origin.applying(CGAffineTransform(translationX: bounds.width * ep1XDelta + firstEndpointXClipCorrection, y: bounds.height * ep1YDelta))
        let secondEndpoint = bounds.origin.applying(CGAffineTransform(translationX: bounds.width * ep2XDelta + secondEndpointXClipCorrection, y: bounds.height * ep2YDelta))

        let firstControlPoint = bounds.origin.applying(CGAffineTransform(translationX: bounds.width * cp1XDelta + firstEndpointXClipCorrection, y: bounds.height * cp1YDelta))
        let secondControlPoint = bounds.origin.applying(CGAffineTransform(translationX: bounds.width * cp2XDelta + secondEndpointXClipCorrection, y: bounds.height * cp2YDelta))

        let bezierPath = UIBezierPath()
        bezierPath.move(to: firstEndpoint)
        bezierPath.addCurve(to: secondEndpoint, controlPoint1: firstControlPoint, controlPoint2: secondControlPoint)
        bezierPath.addLine(to: bounds.origin.applying(CGAffineTransform(translationX: bounds.width, y: 0)))
        bezierPath.addLine(to: bounds.origin.applying(CGAffineTransform(translationX: bounds.width, y: bounds.height)))
        bezierPath.addLine(to: bounds.origin.applying(CGAffineTransform(translationX: 0, y: bounds.height)))
        bezierPath.close()

        tintColor.setFill()
        bezierPath.fill()

        // done
        
        context.saveGState()
    }

}

// MARK: Location coordinate transforms for control-/end- points

extension DomeCurve {

    func cp1Y(progress: CGFloat) -> CGFloat {
        guard progress >= 0 else { return 0 }
        guard progress <= 1 else { return 1 }

        return progress
    }

    func cp2X(progress: CGFloat) -> CGFloat {
        guard progress >= 0 else { return 1 }
        guard progress <= 1 else { return 0 }

        return 1 - progress
    }

    func cp2Y(progress: CGFloat) -> CGFloat {
        guard progress >= 0 else { return 0 }
        guard progress <= 1 else { return 1 }

        return progress
    }

    func ep2X(progress: CGFloat) -> CGFloat {
        guard progress >= 0 else { return 1 }
        guard progress <= 1 else { return 0 }

        return 1 - progress
    }

}
