//
//  ViewController.swift
//  Lozenge
//
//  Created by Andrew McKnight on 10/7/16.
//  Copyright © 2016 Dark Sky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // test 1

    @IBOutlet weak var cp1XSlider: UISlider?
    @IBOutlet weak var cp1YSlider: UISlider?
    @IBOutlet weak var cp2XSlider: UISlider?
    @IBOutlet weak var cp2YSlider: UISlider?
    @IBOutlet weak var ep1XSlider: UISlider?
    @IBOutlet weak var ep1YSlider: UISlider?
    @IBOutlet weak var ep2XSlider: UISlider?
    @IBOutlet weak var ep2YSlider: UISlider?

    @IBOutlet weak var cp1XValueLabel: UILabel!
    @IBOutlet weak var cp1YValueLabel: UILabel!
    @IBOutlet weak var cp2XValueLabel: UILabel!
    @IBOutlet weak var cp2YValueLabel: UILabel!
    @IBOutlet weak var ep1XValueLabel: UILabel!
    @IBOutlet weak var ep1YValueLabel: UILabel!
    @IBOutlet weak var ep2XValueLabel: UILabel!
    @IBOutlet weak var ep2YValueLabel: UILabel!

    // test 2

    @IBOutlet weak var leftDomeCurveView: DomeCurve?
    @IBOutlet weak var rightDomeCurveView: DomeCurve?
    @IBOutlet weak var bottomRightDomeCurveView: DomeCurve?
    @IBOutlet weak var bottomLeftDomeCurveView: DomeCurve?
    @IBOutlet weak var progressSlider: UISlider?
    @IBOutlet weak var progressValueLabel: UILabel!

    @IBOutlet weak var distanceConstraint: NSLayoutConstraint!

}

extension ViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        leftDomeCurveView?.transform = CGAffineTransform(scaleX: 1, y: -1)
        rightDomeCurveView?.transform = CGAffineTransform(scaleX: -1, y: -1)
        bottomRightDomeCurveView?.transform = CGAffineTransform(scaleX: -1, y: 1)
        bottomLeftDomeCurveView?.transform = CGAffineTransform(scaleX: 1, y: 1)

        for sliderOptional: UISlider? in [
            cp1XSlider,
            cp1YSlider,
            cp2XSlider,
            cp2YSlider,
            ep1XSlider,
            ep1YSlider,
            ep2XSlider,
            ep2YSlider
            ] {
                guard let slider = sliderOptional else { continue }
                sliderSlid(slider)
        }
    }

    @IBAction func sliderSlid(_ sender: UISlider) {
        let value: CGFloat = CGFloat(sender.value)
        var label: UILabel? = progressValueLabel

        defer {
            label?.text = NSString(format: "%.2f", value) as String
            view.layoutSubviews()
        }

        for domeCurveOptional: DomeCurve? in [rightDomeCurveView, leftDomeCurveView, bottomRightDomeCurveView, bottomLeftDomeCurveView] {
            guard let domeCurve = domeCurveOptional else { continue }

            // test 1
            if sender == cp1XSlider {
                domeCurve.cp1XDelta = value
                label = cp1XValueLabel
            } else if sender == cp1YSlider {
                domeCurve.cp1YDelta = value
                label = cp1YValueLabel
            } else if sender == cp2XSlider {
                domeCurve.cp2XDelta = value
                label = cp2XValueLabel
            } else if sender == cp2YSlider {
                domeCurve.cp2YDelta = value
                label = cp2YValueLabel
            } else if sender == ep1XSlider {
                domeCurve.ep1XDelta = value
                label = ep1XValueLabel
            } else if sender == ep1YSlider {
                domeCurve.ep1YDelta = value
                label = ep1YValueLabel
            } else if sender == ep2XSlider {
                domeCurve.ep2XDelta = value
                label = ep2XValueLabel
            } else if sender == ep2YSlider {
                domeCurve.ep2YDelta = value
                label = ep2YValueLabel
            } else {
                // test 2
                domeCurve.progress = (value - 0.25) / 0.75
                label = progressValueLabel
            }
            domeCurve.setNeedsDisplay()
        }

        // test 2
        if value >= 0 && value <= 0.25 {
            distanceConstraint?.constant = 200 * (0.25 - value)
        } else {
            distanceConstraint?.constant = 0
        }

    }

}

