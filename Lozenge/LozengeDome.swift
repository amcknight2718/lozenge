//
//  LozengeDome.swift
//  Lozenge
//
//  Created by Andrew McKnight on 10/7/16.
//  Copyright © 2016 Dark Sky. All rights reserved.
//

import UIKit

class LozengeDome: UIView {

    var leftDomeCurve: DomeCurve!
    var rightDomeCurve: DomeCurve!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    convenience init(backgroundColor: UIColor?, tintColor: UIColor) {
        self.init()
        self.backgroundColor = backgroundColor
        self.tintColor = tintColor
        commonInit()
    }
}

// MARK: Public

extension LozengeDome {

    func setProgress(progress: CGFloat) {
        for curve: DomeCurve in [ leftDomeCurve, rightDomeCurve ] {
            curve.progress = progress
            curve.setNeedsDisplay()
        }
    }
    
}

// MARK: Private

extension LozengeDome {

    func commonInit() {
        leftDomeCurve = DomeCurve(backgroundColor: backgroundColor, tintColor: tintColor)
        rightDomeCurve = DomeCurve(backgroundColor: backgroundColor, tintColor: tintColor)

        for view: UIView in [ leftDomeCurve, rightDomeCurve ] {
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(view)
        }

        leftDomeCurve.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        leftDomeCurve.topAnchor.constraint(equalTo: topAnchor).isActive = true
        leftDomeCurve.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        leftDomeCurve.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5).isActive = true

        rightDomeCurve.leadingAnchor.constraint(equalTo: leftDomeCurve.trailingAnchor).isActive = true
        rightDomeCurve.topAnchor.constraint(equalTo: leftDomeCurve.topAnchor).isActive = true
        rightDomeCurve.bottomAnchor.constraint(equalTo: leftDomeCurve.bottomAnchor).isActive = true
        rightDomeCurve.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true

        rightDomeCurve.transform = CGAffineTransform(scaleX: -1, y: 1)
    }

}
